Usage

clone the repo

```
git clone https://gitlab.com/reedrichards/semvarcmp
```

cd into the repo

```
cd semvarcmp
```

build the app and run it

```
gitpod /workspace/semvarcmp (master) $ go build
gitpod /workspace/semvarcmp (master) $ ./myapp semver 1.0.1 1.2.3
-1
gitpod /workspace/semvarcmp (master) $ 
```


also have test cases in the repo

```
gitpod /workspace/semvarcmp (master) $ go test
```

