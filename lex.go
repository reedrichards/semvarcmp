package main

import (
	"fmt"
	"unicode"
	"unicode/utf8"
)

type itemType int

const (
	itemError itemType = iota
	itemNumber
	itemPeriod
	itemEOF
)

type item struct {
	typ itemType
	val string
}

func (i item) String() string {
	switch {
	case i.typ == itemEOF:
		return "EOF"
	case i.typ == itemError:
		return i.val
	}
	return fmt.Sprintf("<%s>", i.val)
}

// a function that returns a statefn
type stateFn func(*lexer) stateFn

type lexer struct {
	name  string    // used in error reports
	input string    // string being scanned
	start int       // start position of this item
	pos   int       // current position of this item
	width int       // width of the last rune read
	items chan item // last scanned item
	state stateFn
}

func lex(name, input string) *lexer {
	l := &lexer{
		name:  name,
		input: input,
		state: lexText,
		items: make(chan item, 2),
	}
	go l.run() // concurrently begin lexing
	return l
}

// synchronously receive an item from lexer
func (l *lexer) nextItem() item {
	return <-l.items
}

func (l *lexer) run() {
	for state := lexText; state != nil; {
		state = state(l)
	}
	close(l.items) // no more tokens will be delivered
}

func (l *lexer) emit(t itemType) {
	l.items <- item{t, l.input[l.start:l.pos]}
	l.start = l.pos
}

const eof = -1

// next returns the next rune in the input.
func (l *lexer) next() rune {
	if int(l.pos) >= len(l.input) {
		l.width = 0
		return eof
	}
	r, w := utf8.DecodeRuneInString(l.input[l.pos:])
	l.width = w
	l.pos += l.width
	return r
}

// peek returns but does not consume the next rune in the input.
func (l *lexer) peek() rune {
	r := l.next()
	l.backup()
	return r
}

// backup steps back one rune. Can only be called once per call of next.
func (l *lexer) backup() {
	l.pos -= l.width
}

func (l *lexer) errorf(format string, args ...interface{}) stateFn {
	l.items <- item{
		itemError,
		fmt.Sprintf(format, args...),
	}
	return nil
}

const (
	period = '.'
)

func lexText(l *lexer) stateFn {
	r := l.next()
	if r == eof {
		l.emit(itemEOF)
		return nil
	}
	if unicode.IsDigit(r) {
		l.backup()
		return lexNumber
	}
	return l.errorf("unexpected character in input: %q", r)
}

func lexNumber(l *lexer) stateFn {
	// consume all digits
	for {
		r := l.next()
		if !unicode.IsDigit(r) {
			l.backup()
			break
		}
	}
	l.emit(itemNumber)
	return lexPeriod // next item should be a period or eof
}

func lexPeriod(l *lexer) stateFn {
	r := l.next()
	if r == period {
		l.emit(itemPeriod)
		if l.peek() == eof {
			l.errorf("unexpected eof after period")
			return nil
		}
		return lexText
	}
	if r == eof {
		l.emit(itemEOF)
		return nil
	}
	return l.errorf("expected period, got %q", r)
}

func parse(content string) ([]item, error) {
	l := lex(content, content)
	var items []item
	for {
		item := l.nextItem()
		if item.typ == itemEOF {
			break
		}
		if item.typ == itemError {
			return nil, fmt.Errorf("error parsing content: %s", item.String())
		}
		items = append(items, item)
	}
	return items, nil
}
