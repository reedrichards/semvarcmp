package main

import (
	"reflect"
	"testing"
)

func Test_parse(t *testing.T) {
	type args struct {
		content string
	}
	tests := []struct {
		name    string
		args    args
		want    []item
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "test1",
			args: args{
				content: "1.0.0",
			},
			want: []item{
				{itemNumber, "1"},
				{itemPeriod, "."},
				{itemNumber, "0"},
				{itemPeriod, "."},
				{itemNumber, "0"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parse(tt.args.content)
			if (err != nil) != tt.wantErr {
				t.Errorf("parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parse() = %v, want %v", got, tt.want)
			}
		})
	}
}
