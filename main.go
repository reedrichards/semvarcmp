package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "cmp semver",
	Short: "compares two semver strings",
	Long: `compares two semver strings and returns the result,
	
	0 if equal
	1 if greater than
	-1 if less than

	For example:
	
	cmp 1.0.0 1.0.0
	0
	`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func main() {
	err := rootCmd.Execute()
	if err != nil {
		// todo capture sentry error
		fmt.Println(err)
		os.Exit(1)
	}
}

func addCommands(cmd *cobra.Command, cmds []*cobra.Command) *cobra.Command {
	for _, c := range cmds {
		cmd.AddCommand(c)
	}
	return cmd
}

func init() {

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.rwcli.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	rootCmd = addCommands(rootCmd, []*cobra.Command{
		{
			Use:   "version",
			Short: "Print the version number of myapp",
			Long:  `All software has versions. This is myapp's`,
			Run: func(cmd *cobra.Command, args []string) {
			},
		},
		{
			Use:   "semver",
			Short: "compares two semver strings",
			Long:  `compares two semver strings and returns the result,`,
			Run: func(cmd *cobra.Command, args []string) {
				if len(args) != 2 {
					fmt.Println("invalid number of arguments")
					os.Exit(1)
				}
				result, err := cmp(args)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				fmt.Println(*result)

			},
		},
	})
}

func cmp(args []string) (*int, error) {
	var argsItems [][]item
	for _, arg := range args {
		argsItem, err := parse(arg)
		if err != nil {
			return nil, err
		}
		argsItems = append(argsItems, argsItem)
	}
	// filter out periods from argitems
	for i, arg := range argsItems {
		var filtered []item
		for _, item := range arg {
			if item.typ != itemPeriod {
				filtered = append(filtered, item)
			}
		}
		argsItems[i] = filtered
	}
	// pad the shorter array with zeros
	if len(argsItems[0]) < len(argsItems[1]) {
		for i := len(argsItems[0]); i < len(argsItems[1]); i++ {
			argsItems[0] = append(argsItems[0], item{itemNumber, "0"})
		}
	}

	if len(argsItems[1]) < len(argsItems[0]) {
		for i := len(argsItems[1]); i < len(argsItems[0]); i++ {
			argsItems[1] = append(argsItems[1], item{itemNumber, "0"})
		}
	}

	// convert the strings to ints
	var intItems [][]int
	for _, arg := range argsItems {
		var intItem []int
		for _, item := range arg {
			if item.typ == itemNumber {
				intVal, err := strconv.Atoi(item.val)
				if err != nil {
					return nil, err
				}
				intItem = append(intItem, intVal)
			}
		}
		intItems = append(intItems, intItem)
	}

	// compare the two arrays
	for i, item := range intItems[0] {
		if item > intItems[1][i] {
			return pint(1), nil
		}
		if item < intItems[1][i] {
			return pint(-1), nil
		}
	}

	return pint(0), nil
}

func pint(i int) *int {
	return &i
}
