package main

import "testing"


func Test_cmp(t *testing.T) {
	type args struct {
		args []string
	}
	tests := []struct {
		name    string
		args    args
		want    *int
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "test1",
			args: args{
				args: []string{"1.0.0", "1.0.0"},
			},
			want:    pint(0),
			wantErr: false,
		},
		{
			name: "test2",
			args: args{
				args: []string{"1.0.0", "1.0.1"},
			},
			want:    pint(-1),
			wantErr: false,
		},
		{
			name: "test3",
			args: args{
				args: []string{"1.0.1", "1.0.0"},
			},
			want:    pint(1),
			wantErr: false,
		},
		{
			name: "test4",
			args: args{
				args: []string{"1.0.0", "1.1.0"},
			},
			want:    pint(-1),
			wantErr: false,
		},
		{
			name: "test5",
			args: args{
				args: []string{"1.1.0", "1.0.0"},
			},
			want:    pint(1),
			wantErr: false,
		},
		{
			name: "test6",
			args: args{
				args: []string{"1.0.0", "122.0.0"},
			},
			want:    pint(-1),
			wantErr: false,
		},
		{
			name: "test invalid input",
			args: args{
				args: []string{"wtflol", "122.0.0"},
			},
			want:    pint(0),
			wantErr: true,
		},
		{
			name: "test different length",
			args: args{
				args: []string{"1.0.0", "1"},
			},
			want:    pint(0),
			wantErr: false,
		},
		{
			name: "test ends with period",
			args: args{
				args: []string{"1.0.0.", "1.0.0"},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "test starts with period",
			args: args{
				args: []string{".1.0.0", "1.0.0"},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "test multiple periods",
			args: args{
				args: []string{"1..0.0", "1.0.0"},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := cmp(tt.args.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("cmp() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				// if this panic thats a failure in the test
				if *got != *tt.want {
					t.Errorf("cmp() = %d, want %d", *got, *tt.want)
				}
			}

		})
	}
}
